package ru.nobirds.kotlin.experiments.aerospike.column

import com.aerospike.client.Bin
import ru.nobirds.kotlin.experiments.aerospike.AerospikeSchema

interface AerospikeSchemaColumn<K, T, S : AerospikeSchema<K, S>> {

    val name: String

    fun readValue(bin: Any?): T

    fun createBin(value: T?): Bin

}