package ru.nobirds.kotlin.experiments.aerospike

import com.esotericsoftware.kryo.Kryo
import ru.nobirds.kotlin.experiments.aerospike.column.AerospikeSchemaColumn
import ru.nobirds.kotlin.experiments.aerospike.column.BeanToMapAerospikeSchemaColumn
import ru.nobirds.kotlin.experiments.aerospike.column.KryoAerospikeSchemaColumn
import ru.nobirds.kotlin.experiments.aerospike.column.SimpleAerospikeSchemaColumn
import kotlin.reflect.KClass

open class AerospikeSchema<K : Any, S : AerospikeSchema<K, S>>(val namespace: String, val setName: String? = null, val kryo: Kryo = Kryo()) {

    fun <T> column(name: String): AerospikeSchemaColumn<K, T, S> = SimpleAerospikeSchemaColumn(name)

    fun <T> serializedColumn(name: String): AerospikeSchemaColumn<K, T, S> = KryoAerospikeSchemaColumn(name, kryo)

    fun <T:Any> beanAsMapColumn(name: String, type:KClass<T>): AerospikeSchemaColumn<K, T, S> = BeanToMapAerospikeSchemaColumn(name, type)

    inline fun <reified T:Any> beanAsMapColumn(name: String): AerospikeSchemaColumn<K, T, S> = beanAsMapColumn(name, T::class)

}