package ru.nobirds.kotlin.experiments.aerospike

import com.aerospike.client.AerospikeException
import com.aerospike.client.Bin
import com.aerospike.client.Key
import com.aerospike.client.Record
import com.aerospike.client.Value
import com.aerospike.client.async.IAsyncClient
import com.aerospike.client.listener.RecordListener
import com.aerospike.client.listener.WriteListener
import com.aerospike.client.policy.Policy
import com.aerospike.client.policy.WritePolicy
import java.util.concurrent.CompletableFuture

class AerospikeBuilder(
        private val client: IAsyncClient,
        private val defaultWritePolicy: WritePolicy = WritePolicy(),
        private val defaultPolicy: Policy = Policy()
) {

    fun <K : Any, S : AerospikeSchema<K, S>, R> S.get(key: K, fetcher: AerospikeRowFetcher<K, S>.(S)->R): CompletableFuture<R> {
        val completableFuture = CompletableFuture<R>()

        val realKey = Key(this.namespace, this.setName, Value.get(key))

        client.get(defaultPolicy, createRecordListener(this, completableFuture, fetcher), realKey)

        return completableFuture
    }

    fun <K : Any, S : AerospikeSchema<K, S>> S.put(key: K, row: AerospikeRowBuilder<K, S>.(S) -> Unit): CompletableFuture<K> {
        return mutate(client::put, this, key, row)
    }

    fun <K : Any, S : AerospikeSchema<K, S>> S.append(key: K, row: AerospikeRowBuilder<K, S>.(S) -> Unit): CompletableFuture<K> {
        return mutate(client::append, this, key, row)
    }

    fun <K : Any, S : AerospikeSchema<K, S>> S.prepend(key: K, row: AerospikeRowBuilder<K, S>.(S) -> Unit): CompletableFuture<K> {
        return mutate(client::prepend, this, key, row)
    }

    private fun <K : Any, S : AerospikeSchema<K, S>> mutate(
            method: (WritePolicy, WriteListener, Key, Array<out Bin>) -> Unit,
            schema: S,
            key: K,
            row: AerospikeRowBuilder<K, S>.(S) -> Unit): CompletableFuture<K> {

        val result = CompletableFuture<K>()

        val rowBuilder = AerospikeRowBuilder(schema, key)

        rowBuilder.row(schema)

        method(defaultWritePolicy, createWriteListener(result), rowBuilder.toKey(), rowBuilder.toBins())

        return result
    }

    private fun <K : Any> createWriteListener(result: CompletableFuture<K>): WriteListener = object : WriteListener {

        override fun onSuccess(key: Key?) {
            result.complete(key?.userKey?.`object` as K)
        }

        override fun onFailure(exception: AerospikeException?) {
            result.completeExceptionally(exception)
        }
    }

    private fun <K : Any, R, S : AerospikeSchema<K, S>> createRecordListener(
            schema: S,
            result: CompletableFuture<R>,
            fetcher: AerospikeRowFetcher<K, S>.(S) -> R
    ): RecordListener = object: RecordListener {

        override fun onSuccess(key: Key, record: Record) {
            result.complete(AerospikeRowFetcher<K, S>(record).fetcher(schema))
        }

        override fun onFailure(exception: AerospikeException) {
            result.completeExceptionally(exception)
        }
    }

}