package ru.nobirds.kotlin.experiments.aerospike.column

import com.aerospike.client.Bin
import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.io.ByteBufferInput
import com.esotericsoftware.kryo.io.ByteBufferOutput
import ru.nobirds.kotlin.experiments.aerospike.AerospikeSchema

class KryoAerospikeSchemaColumn<K, T, S : AerospikeSchema<K, S>>(override val name: String, val kryo: Kryo) : AerospikeSchemaColumn<K, T, S> {

    override fun createBin(value: T?): Bin {
        val byteBufferOutput = ByteBufferOutput()
        kryo.writeClassAndObject(byteBufferOutput, value)
        return Bin(name, byteBufferOutput.buffer)
    }

    override fun readValue(bin: Any?): T {
        val byteBufferInput = ByteBufferInput(bin as ByteArray)
        val classAndObject = kryo.readClassAndObject(byteBufferInput)
        return classAndObject as T
    }

}