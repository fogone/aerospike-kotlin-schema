package ru.nobirds.kotlin.experiments.aerospike

import com.aerospike.client.Bin
import com.aerospike.client.Key
import com.aerospike.client.Value
import ru.nobirds.kotlin.experiments.aerospike.column.AerospikeSchemaColumn

class AerospikeRowBuilder<K, S : AerospikeSchema<K, S>>(val schema: S, val key: K) {

    private val bins = ArrayList<Bin>()

    fun toKey(): Key = Key(schema.namespace, schema.setName, Value.get(key))

    fun toBins(): Array<Bin> = bins.toTypedArray()

    private fun column(bin: Bin) {
        bins.add(bin)
    }

    fun <T> AerospikeSchemaColumn<K, T, S>.set(value: T) {
        column(createBin(value))
    }

    fun <T> AerospikeSchemaColumn<K, T?, S>.setNull() {
        column(createBin(null))
    }

}