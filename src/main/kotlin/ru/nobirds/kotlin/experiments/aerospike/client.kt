package ru.nobirds.kotlin.experiments.aerospike

import com.aerospike.client.async.IAsyncClient


fun IAsyncClient.with(builder: AerospikeBuilder.() -> Unit) {
    AerospikeBuilder(this).builder()
}

