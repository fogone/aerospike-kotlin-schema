package ru.nobirds.kotlin.experiments.aerospike

import com.aerospike.client.Record
import ru.nobirds.kotlin.experiments.aerospike.column.AerospikeSchemaColumn

class AerospikeRowFetcher<K : Any, S : AerospikeSchema<K, S>>(val record: Record) {

    operator fun <T> AerospikeSchemaColumn<K, T, S>.invoke(): T {
        val value = record.bins[this.name]
        return this.readValue(value)
    }

}