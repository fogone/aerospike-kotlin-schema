package ru.nobirds.kotlin.experiments.aerospike.column

import com.aerospike.client.Bin
import ru.nobirds.kotlin.experiments.aerospike.AerospikeSchema
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.memberProperties
import kotlin.reflect.primaryConstructor

class BeanToMapAerospikeSchemaColumn<K:Any, T:Any, S : AerospikeSchema<K, S>>(override val name: String, val type:KClass<T>) : AerospikeSchemaColumn<K, T, S> {

    override fun createBin(value: T?): Bin {
        val map = if (value != null) {
            type.memberProperties.map { it.name to it.get(value) }.toMap()
        } else {
            null
        }

        return Bin(name, map)
    }

    override fun readValue(bin: Any?): T {
        require(bin is Map<*, *>)
        val map = bin as Map<String, Any>

        val primaryConstructor = type.primaryConstructor

        val instance = if (primaryConstructor != null) {
            val parameters = primaryConstructor.parameters.map { it to map[it.name] }.toMap()
            primaryConstructor.callBy(parameters)
        } else {
            type.constructors.first { it.parameters.isEmpty() }.call()
        }

        type.memberProperties
                .filterIsInstance<KMutableProperty1<T, Any?>>()
                .forEach { it.set(instance, map[it.name]) }

        return instance
    }

}