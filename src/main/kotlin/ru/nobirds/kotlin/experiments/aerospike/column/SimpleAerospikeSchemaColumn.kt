package ru.nobirds.kotlin.experiments.aerospike.column

import com.aerospike.client.Bin
import ru.nobirds.kotlin.experiments.aerospike.AerospikeSchema

class SimpleAerospikeSchemaColumn<K, T, S : AerospikeSchema<K, S>>(override val name: String) : AerospikeSchemaColumn<K, T, S> {

    override fun readValue(bin: Any?): T {
        return bin as T
    }

    override fun createBin(value: T?): Bin = Bin(name, value)

}