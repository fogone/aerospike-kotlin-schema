package ru.nobirds.kotlin.experiments

import com.aerospike.client.async.AsyncClient
import com.aerospike.client.async.IAsyncClient
import ru.nobirds.kotlin.experiments.aerospike.AerospikeSchema
import ru.nobirds.kotlin.experiments.aerospike.with

object SimpleSchema : AerospikeSchema<String, SimpleSchema>("some-name-space") {

    val name = column<String>("name")
    val type = column<String>("type")
    val description = column<String?>("description")

    val complex = serializedColumn<ComplexValue>("complex")

    val bean = beanAsMapColumn<ComplexValue>("bean")

}

object SingleSchema : AerospikeSchema<String, SingleSchema>("some-name-space2") {

    val value = serializedColumn<Record>("")

}

data class ComplexValue(val first: String, val second: String)

data class Record(val name: String, val type: String, val description: String?, val complex: ComplexValue, val bean: ComplexValue)

fun main(args: Array<String>) {
    val client:IAsyncClient = AsyncClient("host", 1000)

    client.with {
        SimpleSchema.put("some-key") { schema ->
            schema.name.set("name1")
            schema.type.set("type1")
            schema.description.setNull()

            schema.complex.set(ComplexValue("test", "test2"))
            schema.bean.set(ComplexValue("test2", "test3"))
        }

        SimpleSchema.get("some-key") { schema ->
            Record(
                    schema.name(),
                    schema.type(),
                    schema.description(),
                    schema.complex(),
                    schema.bean()
            )
        }.thenAccept(::println)
    }

    client.with {
        SingleSchema.put("some-key") {
            SingleSchema.value.set(Record("aaa", "bb", "ccc", ComplexValue("dd", "fff"), ComplexValue("ee", "gg")))
        }

        SingleSchema.get("some-key") { it.value() }.thenAccept(::println)
    }

}